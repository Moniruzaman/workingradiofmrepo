//
//  RS_PlayerViewController.m
//  RadioFM
//
//  Created by Rasel_cse07 on 10/26/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "RS_PlayerViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <CFNetwork/CFNetwork.h>
#import "NSString+HTML.h"
#import "MWFeedParser.h"

bool start=false;
bool stop=false;

@interface RS_PlayerViewController ()

@end

@implementation RS_PlayerViewController
@synthesize scroll_Content;
@synthesize itemsToDisplay;
@synthesize indexpathNumber;

CGFloat xPos = 0;
float newPosition;

- (void)viewDidLoad {
    [super viewDidLoad];
    // any additional setup after loading the view from its nib.
    NSLog(@"YouHaveSelectedIndex-----%d",indexpathNumber);
   
    // Setup
    self.title = @"Loading...";
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    parsedItems = [[NSMutableArray alloc] init];
    self.itemsToDisplay = [NSArray array];
    
    // Parse
    NSURL *feedURL = [NSURL URLWithString:@"http://www.thedailystar.net/top-news/rss.xml"];
    feedParser = [[MWFeedParser alloc] initWithFeedURL:feedURL];
    feedParser.delegate = self;
    feedParser.feedParseType = ParseTypeFull; // Parse feed info and all items
    feedParser.connectionType = ConnectionTypeAsynchronously;
    [feedParser parse];
    
    //-----------------
    [[[MyManager sharedManager] streamer] stop];
    [self play];
    [self addScrolView];
    
    if (iPhone6||iPhone6Plus) {
        rssContainerView.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-(BANNER_HEIGHT+rssContainerView.frame.size.height+(iPhone6 ? 30 : 45)), [[UIScreen mainScreen] bounds].size.width, rssContainerView.frame.size.height+(iPhone6 ? 27 : 45));
        scroll_View_Container.frame = CGRectMake(0, rssContainerView.frame.origin.y-scroll_View_Container.frame.size.height, [[UIScreen mainScreen] bounds].size.width, scroll_View_Container.frame.size.height);
        player_Info_Container.frame = CGRectMake(0, scroll_View_Container.frame.origin.y-player_Info_Container.frame.size.height, [[UIScreen mainScreen] bounds].size.width, player_Info_Container.frame.size.height);
        news_Container_ScrollView.frame =CGRectMake(0, news_Container_ScrollView.frame.origin.y, [[UIScreen mainScreen] bounds].size.width, news_Container_ScrollView.frame.size.height+(iPhone6 ? 25 : 45));
    }
    else if (isiPhone4s)
    {
        scroll_View_Container.hidden = YES;
        rssContainerView.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-(BANNER_HEIGHT+rssContainerView.frame.size.height), [[UIScreen mainScreen] bounds].size.width, rssContainerView.frame.size.height);
        //scroll_View_Container.frame = CGRectMake(0, rssContainerView.frame.origin.y-scroll_View_Container.frame.size.height, [[UIScreen mainScreen] bounds].size.width, scroll_View_Container.frame.size.height);
        player_Info_Container.frame = CGRectMake(0, rssContainerView.frame.origin.y-player_Info_Container.frame.size.height, [[UIScreen mainScreen] bounds].size.width, player_Info_Container.frame.size.height);
        news_Container_ScrollView.frame =CGRectMake(0, news_Container_ScrollView.frame.origin.y, [[UIScreen mainScreen] bounds].size.width, news_Container_ScrollView.frame.size.height);
    
    }
    //[news_Container_ScrollView.layer setBorderWidth:2.00];
    //[news_Container_ScrollView.layer setBorderColor:[UIColor greenColor].CGColor];
}

#pragma mark -
#pragma mark Parsing

- (void)updateTableWithParsedItems {
    self.itemsToDisplay = [parsedItems sortedArrayUsingDescriptors:
                           [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"date"
                                                                                ascending:NO]]];
    [self loadNews];
}

#pragma mark -
#pragma mark MWFeedParserDelegate

- (void)feedParserDidStart:(MWFeedParser *)parser {
    NSLog(@"Started Parsing: %@", parser.url);
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedInfo:(MWFeedInfo *)info {
    NSLog(@"Parsed Feed Info: “%@”", info.title);
    self.title = info.title;
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedItem:(MWFeedItem *)item {
    NSLog(@"Parsed Feed Item: “%@”", item.title);
    if (item) [parsedItems addObject:item];
}

- (void)feedParserDidFinish:(MWFeedParser *)parser {
    NSLog(@"Finished Parsing%@", (parser.stopped ? @" (Stopped)" : @""));
    [self updateTableWithParsedItems];
}

- (void)feedParser:(MWFeedParser *)parser didFailWithError:(NSError *)error {
    NSLog(@"Finished Parsing With Error: %@", error);
    if (parsedItems.count == 0) {
        self.title = @"Failed"; // Show failed message in title
    } else {
        // Failed but some items parsed, so show and inform of error
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Parsing Incomplete"
                                                        message:@"There was an error during the parsing of this feed. Not all of the feed items could parsed."
                                                       delegate:nil
                                              cancelButtonTitle:@"Dismiss"
                                              otherButtonTitles:nil];
        [alert show];
    }
    [self updateTableWithParsedItems];
}

-(void)loadNews
{
    // NSLog(itemsToDisplay);
    
    
    CGFloat ypos = 5;
    for (int i=0; i<itemsToDisplay.count; i++) {
        UIViewController *controller=[[UIViewController alloc] initWithNibName:@"RssNewViewCell" bundle:nil];
        RssNewViewCell* view=(RssNewViewCell*)controller.view;
        [view baseInit];
        [view setFrame:CGRectMake(0, ypos, [[UIScreen mainScreen] bounds].size.width, view.frame.size.height)];
        
        MWFeedItem *item = [itemsToDisplay objectAtIndex:i];
        
        if (is_Debug) {
            [view.layer setBorderWidth:2.00];
            [view.layer setBorderColor:[UIColor redColor].CGColor];
             NSLog(@"%@",item.title);
        }
        
        if (iPhone6||iPhone6Plus) {
            [view.headLine setFrame:CGRectMake(view.headLine.frame.origin.x, view.headLine.frame.origin.y, rssContainerView.frame.size.width, view.headLine.frame.size.height)];
            [view.detailsText setFrame:CGRectMake(view.detailsText.frame.origin.x, view.detailsText.frame.origin.y, rssContainerView.frame.size.width, view.detailsText.frame.size.height)];
        }
        
        [view.headLine setText:[NSString stringWithFormat:@"%@",item.title]];
        [view.detailsText setText:[NSString stringWithFormat:@"%@",item.summary]];
        if (item.date) [view.DateLabel setText:[NSString stringWithFormat:@"%@",[formatter stringFromDate:item.date]]];
        
        ypos += view.frame.size.height+5;
        [news_Container_ScrollView addSubview:view];
    }
    float sHeight = CGRectGetHeight(news_Container_ScrollView.frame);
    float Position = news_Container_ScrollView.contentOffset.y+ypos;
    
    CGSize scrollableSize = CGSizeMake(sHeight,Position);
    [news_Container_ScrollView setContentSize:scrollableSize];
    
    //[news_Container_ScrollView setBackgroundColor:[UIColor greenColor]];
}

-(void)addScrolView
{
    CGFloat width = 125;
    CGFloat height = 38;
    CGFloat xpos = 8;
    CGFloat ypos = 7;
    for (NSString *s in FM_Chanell_Array) {
        UITextField *txt = [[UITextField alloc] initWithFrame:CGRectMake(xpos, ypos, width, height)];
        txt.font = [UIFont fontWithName:@"Helvetica.ttf" size:12];
        [txt setTextColor:[UIColor lightTextColor]];
        [txt setText:s];
        [txt setEnabled:NO];
        [txt.layer setBorderWidth:3.0];
        [txt.layer setBorderColor:[UIColor grayColor].CGColor];
        txt.textAlignment = NSTextAlignmentCenter;
        xpos += width;
        [scroll_Content addSubview:txt];
    }
    
    float sHeight = CGRectGetHeight(scroll_Content.frame);
    newPosition = scroll_Content.contentOffset.x+xpos;
    
    CGSize scrollableSize = CGSizeMake(newPosition, sHeight);
    [scroll_Content setContentSize:scrollableSize];
    
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];

}
- (void) onTimer {
    [scroll_Content setContentOffset:CGPointMake(xPos,scroll_Content.contentOffset.y)
                            animated:YES];
    xPos += 100;
    if (xPos > (newPosition-320)) {
        xPos = 0;
    }
    
}
- (IBAction)dismissViewAction:(id)sender
{
    [streamer stop];
    [self destroyStreamer];
    [[MyManager sharedManager] setSelectedIndex:-1];
    [self.navigationController popViewControllerAnimated:YES];
    
    /*
    [self dismissViewControllerAnimated:YES completion:^{
    
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    }];
     */
}

- (IBAction)nextTrackAction:(id)sender {
    
    if (indexpathNumber<24) {
        
        indexpathNumber++;
        [streamer stop];
        [self destroyStreamer];
        [self play];
    }
}

- (IBAction)previousTrackAction:(id)sender {
    
    if (indexpathNumber> 0)
    {
        indexpathNumber--;
        [streamer stop];
        [self destroyStreamer];
        [self play];
        
    }
}

- (IBAction)menuTrackAction:(id)sender {
    
  
}

- (IBAction)reloadPlayerAction:(id)sender {
    
    [streamer stop];
    [self destroyStreamer];
    [self play];
}

- (IBAction)menuBackAction:(id)sender {
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)play
{
    chanellName.text=[NSString stringWithFormat:@"%@",[FM_Chanell_Array objectAtIndex:indexpathNumber]];
    if([[AppDelegate shareDelegate] checkForConnectivity]) {
       
        [self createStreamer:[[AppDelegate shareDelegate] getChanellUrlFromName:indexpathNumber]];
    
    } else {
        
        return;
    }
}
#pragma mark-Audio streaming
- (void)createStreamer:(NSString*)radioLink

{
    
    NSLog(@"Stream");
    if (streamer)
    {
        
        return;
    }
    
    NSString *escapedValue =
    [(NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                         nil,
                                                         (CFStringRef)radioLink,
                                                         NULL,
                                                         NULL,
                                                         kCFStringEncodingUTF8)
     autorelease];
    
    NSURL *url = [NSURL URLWithString:escapedValue];
    streamer = [[AudioStreamer alloc] initWithURL:url];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(playbackStateChanged:)
     name:ASStatusChangedNotification
     object:streamer];
    
    [streamer start];
    [[MyManager sharedManager] setStreamer:streamer];
    
}

- (void)destroyStreamer
{
   
    if (streamer)
    {
        [[NSNotificationCenter defaultCenter]
         removeObserver:self
         name:ASStatusChangedNotification
         object:streamer];
        [streamer stop];
        streamer = nil;
    }
}
- (void)playbackStateChanged:(NSNotification *)aNotification
{
    if ([streamer isWaiting])
    {
        //[self setButtonImageNamed:@"loadingbutton.png"];
    }
    else if ([streamer isPlaying])
    {
       
    }
    else if ([streamer isIdle])
    {
        [self destroyStreamer];
        
    }else if ([streamer isProxy]){
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
