//
//  RS_ChanellTableCell.m
//  RadioFM
//
//  Created by Rasel_cse07 on 10/26/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "RS_ChanellTableCell.h"

@implementation RS_ChanellTableCell
@synthesize chanellName,chanellThumbImage;

- (void)awakeFromNib {
    // Initialization code
    [chanellThumbImage.layer setCornerRadius:5.0];
    [chanellThumbImage setClipsToBounds:YES];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
