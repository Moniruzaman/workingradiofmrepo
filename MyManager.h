//
//  MyManager.h
//  RadioFM
//
//  Created by SM Moniruzaman on 12/27/15.
//  Copyright © 2015 Rasel_cse07. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AudioStreamer;
@interface MyManager : NSObject{

    NSInteger selectedIndex;
}

@property NSInteger selectedIndex;
@property(nonatomic,retain)AudioStreamer *streamer;

+(id)sharedManager;

@end
