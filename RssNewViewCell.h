//
//  RssNewViewCell.h
//  RadioFM
//
//  Created by SM Moniruzaman on 12/30/15.
//  Copyright © 2015 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RssNewViewCell : UIView
{

}
@property (weak, nonatomic) IBOutlet UILabel *headLine;
@property (weak, nonatomic) IBOutlet UILabel *detailsText;
@property (weak, nonatomic) IBOutlet UILabel *DateLabel;

-(void)baseInit;
@end
