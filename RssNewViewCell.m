//
//  RssNewViewCell.m
//  RadioFM
//
//  Created by SM Moniruzaman on 12/30/15.
//  Copyright © 2015 Rasel_cse07. All rights reserved.
//

#import "RssNewViewCell.h"
#import "PrefixHeader.pch"
@implementation RssNewViewCell

@synthesize headLine;
@synthesize detailsText;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)baseInit
{
    if (is_Debug) {
        [headLine.layer setBorderWidth:2.00];
        [headLine.layer setBorderColor:[UIColor yellowColor].CGColor];
        [detailsText.layer setBorderWidth:2.00];
        [detailsText.layer setBorderColor:[UIColor greenColor].CGColor];
    }
  [headLine setFont:[UIFont fontWithName:@"Helvetica Neue-Bold" size:18]];
  [detailsText setFont:[UIFont fontWithName:@"Helvetica Neue" size:12]];
  detailsText.text = @"Details Text Details Text Details Text Details Text Details Text Details Text Details Text Details Text Details Text Details Text Details Text Details Text Details Text Details Text";
}


@end
