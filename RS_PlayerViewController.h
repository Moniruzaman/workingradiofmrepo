//
//  RS_PlayerViewController.h
//  RadioFM
//
//  Created by Rasel_cse07 on 10/26/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AudioStreamer.h"
#import "AppDelegate.h"
#import "MyManager.h"
#import "PrefixHeader.pch"
#import "RssNewViewCell.h"
#import "MWFeedParser.h"

@class AudioStreamer;
@class GADBannerView;

@interface RS_PlayerViewController : UIViewController<MWFeedParserDelegate>
{

    // Parsing
    MWFeedParser *feedParser;
    NSMutableArray *parsedItems;
    
    // Displaying
    NSArray *itemsToDisplay;
    NSDateFormatter *formatter;
    
    AudioStreamer *streamer;
    __weak IBOutlet UILabel *chanellName;
    __weak IBOutlet UIButton *menuView;
    __weak IBOutlet UIView *rssContainerView;
    __weak IBOutlet UIView *scroll_View_Container;
    __weak IBOutlet UIView *player_Info_Container;
    __weak IBOutlet UIScrollView *news_Container_ScrollView;
    
    
}

// Properties
@property (nonatomic, strong) NSArray *itemsToDisplay;

@property(assign)int indexpathNumber;
@property (nonatomic,retain) IBOutlet UIScrollView *scroll_Content;
- (IBAction)dismissViewAction:(id)sender;
- (IBAction)nextTrackAction:(id)sender;
- (IBAction)previousTrackAction:(id)sender;
- (IBAction)menuTrackAction:(id)sender;
- (IBAction)reloadPlayerAction:(id)sender;

- (IBAction)menuBackAction:(id)sender;
@end
