//
//  RS_HeaderFile.h
//  RadioFM
//
//  Created by Rasel_cse07 on 10/26/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#ifndef RadioFM_RS_HeaderFile_h
#define RadioFM_RS_HeaderFile_h
//#import "GADBannerView.h"
//#import "GADRequest.h"


#import "Reachability.h"
#define  MY_BANNER_UNIT_ID @"ca-app-pub-1429768835145587/6675063151"


#define RadioToday_Url                          @"http://220.247.162.146:7170";
#define DhakaFM_Url                             @"http://118.179.219.244:8000/";
#define ABCRadio_Url                            @"http://live.abcradiobd.fm:8282/;stream.mp3";
#define AKTARA_Url                              @"http://50.7.98.106:8520/;stream.mp3";
#define BanglaNews24_Url                        @"http://118.179.203.140:8877";
#define BanglaRadio24_Url                       @"http://live.banglaradio24.com:8237/;stream.mp3";
#define BanglaWadio_Url                         @"http://66.85.136.195:8201/;stream.mp3";
#define BangladeshBetar_Url                     @"http://108.178.13.122:8081/;stream.mp3";
#define BhootFM_Url                             @"http://121.200.62.53:1021/;stream.mp3";
#define EBangaliRadio_Url                       @"http://banglaradio.homeip.net:8000/;stream.mp3";
#define EDoctorRadio_Url                        @"http://184.154.202.243:8139/;stream.mp3";
#define ETune24_Url                             @"http://184.107.144.218:8040/;stream.mp3";
#define HelloDhaka_Url                          @"http://184.154.145.114:8000/stream?type=.mp3/;stream.mp3";
#define Lemon24_Url                             @"http://office.mcc.com.bd:8000/;stream.mp3";
#define MyBanglaGaan_Url                        @"http://69.175.103.230:8000/;stream.mp3";
#define Onubhuti_Url                            @"http://50.7.98.106:8520/;stream.mp3";
#define Radio2Fun_Url                           @"http://107.178.110.114:7600/;stream.mp3";
#define RadioAmar_Url                           @"http://192.184.9.158:8343/live?type=.mp3/;stream.mp3";
#define RadioAmader_Url                         @"http://192.99.147.218:9998/;stream.mp3";
#define RadioApon_Url                           @"http://118.179.219.244:8000/;stream.mp3";
#define RadioBornomala_Url                      @"http://192.99.147.218:9998/;stream.mp3";
#define RadioChowa_Url                          @"http://198.27.80.37:5484/;stream.mp3";
#define RadioBoss24_Url                         @"http://streams.museter.com:8311/;stream.mp3";
#define RadioCircle_Url                         @"http://66.85.136.195:8019/;stream.mp3";
#define RadioDhaka_url                          @"http://118.179.219.243:8000/;stream.mp3";


#define FM_Chanell_Array [NSArray arrayWithObjects:@"RadioToday", @"DhakaFM",@"ABCRadio",@"AKTARA",@"BanglaNews24",@"BanglaRadio24",@"BanglaWadio",@"BangladeshBetar",@"BhootFM",@"EBangaliRadio",@"EDoctorRadio",@"ETune24",@"HelloDhaka",@"Lemon24",@"MyBanglaGaan",@"Onubhuti",@"Radio2Fun",@"RadioAmar",@"RadioAmader",@"RadioApon",@"RadioBornomala",@"RadioChowa",@"RadioBoss24",@"RadioCircle",@"RadioDhaka",nil]


#endif
