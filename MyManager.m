//
//  MyManager.m
//  RadioFM
//
//  Created by SM Moniruzaman on 12/27/15.
//  Copyright © 2015 Rasel_cse07. All rights reserved.
//

#import "MyManager.h"

@implementation MyManager

@synthesize selectedIndex;

#pragma mark Singleton Methods

/*
+(id)sharedManager
{
    static MyManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        sharedMyManager = [[self alloc] init];
    });

    return sharedMyManager;
}
 */

-(id)init
{
    if (self = [super init]) {
       //initialize default value
    }
    return self;
}

#pragma mark Singleton Methods without GCD dispatch
+(id)sharedManager
{
    static MyManager *sharedMyManager = nil;
    @synchronized(self) {
        if (sharedMyManager == nil) {
            sharedMyManager = [[self alloc] init];
        }
    }
    return sharedMyManager;
}


@end
