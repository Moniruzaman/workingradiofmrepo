//
//  RS_ChanellTableCell.h
//  RadioFM
//
//  Created by Rasel_cse07 on 10/26/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RS_ChanellTableCell : UITableViewCell
{
}
@property (weak, nonatomic) IBOutlet UILabel *chanellName;
@property (weak, nonatomic) IBOutlet UIImageView *chanellThumbImage;
@property (weak, nonatomic) IBOutlet UIImageView *playingView;


@end
