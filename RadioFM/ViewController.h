//
//  ViewController.h
//  RadioFM
//
//  Created by Rasel_cse07 on 10/26/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RS_ChanellTableCell.h"
#import "RS_PlayerViewController.h"
#import "MyManager.h"

@import GoogleMobileAds;

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *channelTable;
    MyManager *sharedManager;

}

@end

