//
//  AppDelegate.m
//  RadioFM
//
//  Created by Rasel_cse07 on 10/26/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "AppDelegate.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //sleep(20);
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    // Override point for customization after application launch.
    ViewController *view=[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:view];
    self.window.rootViewController=nav;
    [self.window makeKeyAndVisible];
    
    CGFloat height_Banner = 50.0;
    bannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-height_Banner, [[UIScreen mainScreen] bounds].size.width, height_Banner)];
    //[bannerView setBackgroundColor:[UIColor greenColor]];
    [self.window addSubview:bannerView];
    bannerView.adUnitID = @"ca-app-pub-7117592313772627/5577700998";
    //bannerView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
    bannerView.rootViewController = nav;
    [bannerView loadRequest:[GADRequest request]];
    
    return YES;
}


+(AppDelegate*)shareDelegate{
    
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
}
- (void)customAlert:(NSString*)title1 withMessage:(NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(title1, title1) message:NSLocalizedString(message, message) delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                          otherButtonTitles:nil];
    [alert show];
    
}
// checking internet
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

// checking internet
- (BOOL)checkForConnectivity {
    
    if (![self connected]) {
        
        [self customAlert:@"No Internet!" withMessage:@"Internet Conncetion is Needed"];
        return NO;
    }
    
    return YES;
}
-(NSString *)getChanellUrlFromName:(int)indepathNumber
{
    NSString *name;
    switch (indepathNumber) {
        
        case 0:
        {
            name=RadioToday_Url;
        }break;
        case 1:
        {
            name=DhakaFM_Url;
        }break;
        case 2:
        {
            name=ABCRadio_Url;
        }break;
        case 3:
        {
            name=AKTARA_Url;
        }break;
        case 4:
        {
            name=BanglaNews24_Url;
        }break;
        case 5:
        {
            name=BanglaRadio24_Url;
        }break;
        case 6:
        {
            name=BanglaWadio_Url;
        }break;
        case 7:
        {
            name=BangladeshBetar_Url;
        }break;
        case 8:
        {
            name=BhootFM_Url;
        }break;
        case 9:
        {
            name=EBangaliRadio_Url;
        }break;
        case 10:
        {
            name=EDoctorRadio_Url;
        }break;
        case 11:
        {
            name=ETune24_Url;
        }break;
        case 12:
        {
            name=HelloDhaka_Url;
        }break;
        case 13:
        {
            name=Lemon24_Url;
        }break;
        case 14:
        {
            name=MyBanglaGaan_Url;
        }break;
        case 15:
        {
            name=Onubhuti_Url;
        }break;
        case 16:
        {
            name=Radio2Fun_Url;
        }break;
        case 17:
        {
            name=RadioAmar_Url;
        }break;
        case 18:
        {
            name=RadioAmader_Url;
        }break;
        case 192:
        {
            name=RadioApon_Url;
        }break;
        case 20:
        {
            name=RadioBornomala_Url;
        }break;
        case 21:
        {
            name=RadioChowa_Url;
        }break;
        case 22:
        {
            name=RadioBoss24_Url;
        }break;
        case 23:
        {
            name=RadioCircle_Url;
        }break;
        case 24:
        {
            name=RadioDhaka_url;
        }break;
            
        default:
            break;
    }
    
    NSLog (@"The RequiredUrl is---> %@", name);

    return name;
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
