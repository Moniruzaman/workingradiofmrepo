//
//  AppDelegate.h
//  RadioFM
//
//  Created by Rasel_cse07 on 10/26/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "RS_HeaderFile.h"

@import GoogleMobileAds;
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    GADBannerView  *bannerView;
}
@property (strong, nonatomic) UIWindow *window;

- (void)customAlert:(NSString*)title1 withMessage:(NSString*)message;
- (BOOL)checkForConnectivity;
-(NSString *)getChanellUrlFromName:(int)indepathNumber;
+ (AppDelegate*)shareDelegate;

@end

