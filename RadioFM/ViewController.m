//
//  ViewController.m
//  RadioFM
//
//  Created by Rasel_cse07 on 10/26/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "ViewController.h"
#import "PrefixHeader.pch"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    sharedManager = [MyManager sharedManager];
    self.navigationController.navigationBarHidden=YES;
    [channelTable setBackgroundColor:[UIColor clearColor]];
    [channelTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
}
-(void)viewDidAppear:(BOOL)animated{

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if(is_Debug)
         NSLog(@"Debug Mode");
    else
         NSLog(@"Debug Mode No");
 
}
#pragma mark - Table View Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [FM_Chanell_Array count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 77;
    
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    RS_ChanellTableCell *cell = [table dequeueReusableCellWithIdentifier:@"RS_ChanellTableCell"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RS_ChanellTableCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    cell.chanellName.text=[NSString stringWithFormat:@"%@",[FM_Chanell_Array objectAtIndex:indexPath.row]];
    [cell.chanellThumbImage setImage:[UIImage imageNamed:@"thumb_Img"]];

    if ([cell.playingView isAnimating]) {
        //cell.playingView.hidden = NO;
    }
    else
    {
        cell.playingView.animationImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"DynamicAnimation1.png"],[UIImage imageNamed:@"DynamicAnimation2.png"],[UIImage imageNamed:@"DynamicAnimation3.png"],[UIImage imageNamed:@"DynamicAnimation4.png"],[UIImage imageNamed:@"DynamicAnimation5.png"],[UIImage imageNamed:@"DynamicAnimation6.png"],nil];
        cell.playingView.animationDuration = 1.0f;
        cell.playingView.animationRepeatCount = 0;
        //cell.playingView.hidden = YES;
    }
    
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}
- (void)tableView:(UITableView *)table didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RS_PlayerViewController *playView=[[RS_PlayerViewController alloc] initWithNibName:@"RS_PlayerViewController" bundle:nil];
    [playView setIndexpathNumber:indexPath.row];
    
    NSLog(@"--------->>>>>%d",indexPath.row);
    NSLog(@"--------->>>>>%d",[[MyManager sharedManager] selectedIndex]);
    
    /*
    if (indexPath.row != [[MyManager sharedManager] selectedIndex]&&([[MyManager sharedManager] selectedIndex] > 0)) {
        NSArray *anArrayOfIndexPath = [NSArray arrayWithArray:[channelTable indexPathsForVisibleRows]];
        NSIndexPath *indexPath= [anArrayOfIndexPath objectAtIndex:[[MyManager sharedManager] selectedIndex]];
        RS_ChanellTableCell *cell = [table cellForRowAtIndexPath:indexPath];
        [cell.playingView stopAnimating];
        //cell.playingView.hidden = YES;
        
    }
    RS_ChanellTableCell *selectedCell = [table cellForRowAtIndexPath:indexPath];
    //selectedCell.playingView.hidden = NO;
    [selectedCell.playingView startAnimating];
     */
    
    [sharedManager setSelectedIndex:indexPath.row];
    [self.navigationController pushViewController:playView animated:YES];

}
//-----
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)bannerRequest
{
   
}
@end
